love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "SubTile", "Minimal"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = (256 * 4)
   t.window.height = (256 * 4)
   t.window.vsync = false
   t.version = "11.4"
end
