uniform float scale;
uniform float grid_size;
uniform float offset_x;
uniform float offset_y;

vec4 effect(vec4 color, Image tex, 
            vec2 texture_coords,
            vec2 screen_coords)
{
    float grid_size_scaled = grid_size * scale;
    float ox = offset_x * scale;
    float oy = offset_y * scale;
    if ( !(int(mod(float(screen_coords.x - ox), float(grid_size_scaled))) == 0 ||
	 int(mod(float(screen_coords.y - oy), float(grid_size_scaled))) == 0)
	 ){
      color=vec4(0.0,0.0,0.0,0.0);
    }
    return color;
}
