(local state (require :state))

(local fennel (require :lib.fennel))
(local repl (require :lib.stdio))

(local subtile (require :subtile))
(local grid (require :grid))

(import-macros {: hex} :hex)

(tset state :grid-shader (love.graphics.newShader :grid.glsl))

(fn love.load [args]
  (love.graphics.setDefaultFilter :nearest :nearest)
  (when (~= :web (. args 1)) (repl.start))
  (tset state :tileset (subtile.make-simple-square-tileset :grass-auto-tile.png 8))
  (state.g 4 4 1)
  (state.g:apply (fn [v i j] (state.auto:set i j (state.g:neighbors i j)))))


(local camera {:x 0 :y 0 :scale 4})

(fn love.draw []
  (local lg love.graphics)
  (local {: grid-shader} state)
  (lg.push :all)
  (lg.clear (hex :ffecd6))

  (lg.setColor (hex :8d697a))
  ;; (state.g:apply
  ;;  (fn [v i j _data _key]
  ;;    (when (= v 1)
  ;;      (lg.rectangle :fill
  ;;                    (+ (* camera.x camera.scale) (* 16 camera.scale i))
  ;;                    (+ (* camera.scale camera.y) (* 16 camera.scale j))
  ;;                    (* camera.scale 16) (* camera.scale 16)))))


  (lg.setColor (hex :ffffff))
  (state.auto:apply
   (fn [v i j _data _key]     
     (when (> v 0)
       (let [quad-names (. state.tileset.unique-quad-layouts (. state.tileset.unique-quad-layouts-map v))
             quads state.tileset.quads]
         ;; (pp quad-names)
         (io.flush)
         (lg.draw state.tileset.image (. quads (. quad-names 1) 1)
                  (+ (* camera.x camera.scale) (* 16 camera.scale i))
                  (+ (* camera.scale camera.y) (* 16 camera.scale j)) 0 camera.scale camera.scale)
         (lg.draw state.tileset.image (. quads (. quad-names 2) 1)
                  (+ (* 16 0.5 camera.scale) (* camera.x camera.scale) (* 16 camera.scale i))
                  (+ (* camera.scale camera.y) (* 16 camera.scale j)) 0 camera.scale camera.scale)
         (lg.draw state.tileset.image (. quads (. quad-names 3) 1)
                  (+ (* 16 0.5 camera.scale) (* camera.x camera.scale) (* 16 camera.scale i))
                  (+ (* 16 0.5 camera.scale) (* camera.scale camera.y) (* 16 camera.scale j)) 0 camera.scale camera.scale)
         (lg.draw state.tileset.image (. quads (. quad-names 4) 1)
                  (+ (* camera.x camera.scale) (* 16 camera.scale i))
                  (+ (* 16 0.5 camera.scale) (* camera.scale camera.y) (* 16 camera.scale j)) 0 camera.scale camera.scale)
         ))))
  
  (local (mx my) (love.mouse.getPosition))
  (local (i j)
         (values (-> mx (- (* camera.x camera.scale)) (/ camera.scale) (/ 16) (math.floor))
                 (-> my (- (* camera.y camera.scale)) (/ camera.scale) (/ 16) (math.floor))))

  (lg.setColor (hex :ffaa5e))
  (lg.print (string.format "%s,%s" i j)
            (+ (* (+ i 0) 16 camera.scale) (* camera.x camera.scale))
            (+ (* (+ j 0) 16 camera.scale) (* camera.y camera.scale)))
  (lg.circle :line
             (+ (* (+ i 0.5) 16 camera.scale) (* camera.x camera.scale))
             (+ (* (+ j 0.5) 16 camera.scale) (* camera.y camera.scale))
             (* 4 camera.scale))
  
  (lg.setShader grid-shader)
  (when (grid-shader:hasUniform :scale) (grid-shader:send :scale camera.scale))
  (when (grid-shader:hasUniform :grid_size) (grid-shader:send :grid_size 16))
  (when (grid-shader:hasUniform :offset_x) (grid-shader:send :offset_x camera.x))
  (when (grid-shader:hasUniform :offset_y) (grid-shader:send :offset_y camera.y))
  (lg.setColor (hex :0d2b45))
  (local (w h) (love.window.getMode))
  (lg.rectangle :fill 0 0 w h)
  ;; (lg.setShader)
  (lg.pop)
  ;; (subtile.draw-unique-tileset state.tileset.image state.tileset.quads state.tileset.unique-quad-layouts state.tileset.unique-quad-layouts-map)
  )

(fn love.update [dt]
  (state.g:apply (fn [v i j] (state.auto:set i j (state.g:neighbors i j)))))

(fn love.keypressed [key]
  )

(fn pan-generator [camera]
  (var _sx 0)
  (var _sy 0)
  (var _cx 0)
  (var _cy 0)
  (var _x 0)
  (var _y 0)
  (var _active false)    
  {:start (fn [x? y?]
            (set _sx (or x? 0))
            (set _sy (or y? 0))
            (set _x (or x? 0))
            (set _y (or y? 0))
            (set _cx camera.x)
            (set _cy camera.y)
            (set _active true)
            (values (- _sx _x) (- _sy _y)))
   :end (fn [x? y?]
             (set _x (or x? 0))
             (set _y (or y? 0))
             (set camera.x (- _cx (-> (- _sx _x) (/ camera.scale))))
             (set camera.y (- _cy (-> (- _sy _y) (/ camera.scale))))
             (set _active false)
             (values (- _sx _x) (- _sy _y)))
   :update (fn [x? y?]
             (when _active
               (set _x (or x? 0))
               (set _y (or y? 0))
               (set camera.x (- _cx (-> (- _sx _x) (/ camera.scale))))
               (set camera.y (- _cy (-> (- _sy _y) (/ camera.scale)))))
             (values (- _sx _x) (- _sy _y)))
   :get (fn [] (values (- _sx _x) (- _sy _y)))
   :is-active? (fn [] _active)})

(fn draw-generator [camera grid]
  (var _active false)
  (fn real->game [mx my]
         (values (-> mx (- (* camera.x camera.scale)) (/ camera.scale) (/ 16) (math.floor))
                 (-> my (- (* camera.y camera.scale)) (/ camera.scale) (/ 16) (math.floor))))
  {:start (fn [x y]
            (let [(i j) (real->game x y)]
              (grid:set i j 1)
              (set _active true)))
   :end (fn [_x _y]
          (set _active false))
   :update (fn [x y]
             (when _active
               (let [(i j) (real->game x y)]
                 (grid:set i j 1))))
   :is-active? (fn [] _active)})

(tset state :g (grid.new 100 100))
(tset state :auto (grid.new 100 100))
(local action (draw-generator camera state.g))

(local pan (pan-generator camera))

(fn love.wheelmoved [x y]
  (let [p camera.scale
        n (math.max 0.1 (* camera.scale (+ 1 (* 0.1 y)) ))
        (mx my) (love.mouse.getPosition)
        ox  (* mx  (/ (- n p) (* p n)))
        oy  (* my  (/ (- n p) (* p n)))
        ]
    (when (not (or (pan.is-active?) (action.is-active?)))
      (set camera.scale n)
      (set camera.x (- camera.x ox))
      (set camera.y (- camera.y oy)))))

(fn love.mousepressed [x y button]
  (var button-name (. [:left :right :middle] button))
  (when (and  (= button-name :left) (love.keyboard.isDown :space)) (set button-name :middle))
  (match button-name
    :left (action.start x y)
    ;; :right (context-menu x y)
    :middle (pan.start x y))
  )

(fn love.mousereleased [x y button]
  (var button-name (. [:left :right :middle] button))
  (when (and  (= button-name :left) (love.keyboard.isDown :space)) (set button-name :middle))
  (match button-name
    :left (action.end x y)
    ;; :right :nothing
    :middle (pan.end x y)))

(fn love.mousemoved [x y]
  (action.update x y)
  (pan.update x y))

