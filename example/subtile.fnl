;; Title: SubTile
;; Description:
;; Author: AlexJGriffith
;; Licence: MIT
;; Version: 0.2.1

(fn truth-table-row [column-number size start?]
  (let [range (math.floor (/ (math.log10 size) (math.log10 2)))]
    (fcollect [i (or start? 0) range] (-> column-number (/ (^ 2 i)) (math.floor) (% 2) (= 1)))))

;; (make-quads :grass-auto-tile.png 8)
(fn make-simple-square-tileset [image-file n]
  (local neighbor-order [:top-right :top :top-left :left :bottom-left :bottom :bottom-right :right :center])
  (local image (love.graphics.newImage image-file))
  (local (iw ih) (image:getDimensions))  
  (fn make-quad [w h]
    (fn [i j] (love.graphics.newQuad (* i w) (* j h) w h iw ih)))
  (local make-quad-n (make-quad n n))
  (fn make-quads-n [...]
    (icollect [_ [i j] (ipairs [...])]
      (make-quad-n i j)))
  (local quads
         {:total                   (make-quads-n [6 0])
          :outer-top-right         (make-quads-n [0 0])
          :outer-top-right-back    (make-quads-n [1 1])
          :outer-top-left          (make-quads-n [5 0])
          :outer-top-left-back     (make-quads-n [4 1])
          :outer-bottom-right      (make-quads-n [0 5])
          :outer-bottom-right-back (make-quads-n [1 4])  
          :outer-bottom-left       (make-quads-n [5 5])
          :outer-bottom-left-back  (make-quads-n [4 4])  
          :inner-bottom-left       (make-quads-n [2 2])
          :inner-bottom-right      (make-quads-n [3 2])
          :inner-top-left          (make-quads-n [2 3])
          :inner-top-right         (make-quads-n [3 3])
          :right                   (make-quads-n [0 1] [0 2] [0 3] [0 4])
          :right-back              (make-quads-n [1 2] [1 3])
          :left                    (make-quads-n [5 1] [5 2] [5 3] [5 4])
          :left-back               (make-quads-n [4 2] [4 3])  
          :top                     (make-quads-n [1 0] [2 0] [3 0] [4 0])
          :top-back                (make-quads-n [2 1] [3 1])  
          :bottom                  (make-quads-n [1 5] [2 5] [3 5] [4 5]) 
          :top-back                (make-quads-n [2 4] [3 4])
  })
  (local corner-neighbor-order
         {:top-right [:right :top :top-right :center]
          :top-left [:left :top  :top-left :center]
          :bottom-left [:left :bottom :bottom-left :center]
          :bottom-right [:right :bottom :bottom-left :center]})
  
  (fn make-corner-mapping-simple [top left]
    [(.. "outer-" top "-" left) top left (.. "inner-" top "-" left)
     (.. "outer-" top "-" left) top left :total
     :empty :empty :empty :empty :empty :empty :empty :empty]) ;; for the case where the center is false

  (fn zip [hash-name hash-values]
    "(zip [:a :b :c] [false false false])"
    (collect [index value (ipairs hash-name)]
      (values value (. hash-values index))))

  (fn power-two [indexes hash]
    "(power-two [:a :b :c] {:a false :b false :c false})"
    (accumulate [acc 0 n index (ipairs indexes)]
      (if (. hash index)
          (+ acc (^ 2 (- n 1)))
          acc)))
  
  (fn dedupe-array [array]
    (let [unique []
          temp-unique {}
          map []
          fennel (require :lib.fennel)]
      (each [index value (ipairs array)]
        (let [key (fennel.view value)
              temp-index (. temp-unique key)]
          (if temp-index
              (table.insert map temp-index)
              (do
                (table.insert unique value)
                (tset temp-unique key (# unique))
                (table.insert map (# unique))
                ))))
      (values unique map)))
  
  (local quad-maps (fcollect [i 0 255 1]
    (let [truth-row (zip neighbor-order (truth-table-row i 256))]         
      ;; (each [key value (a:gmatch "[^-]+")] key)
      (icollect [_ corner (ipairs [:top-right :top-left :bottom-left :bottom-right])]
        (let [(top left) ((corner:gmatch "(%w+)-(%w+)"))
              corner-mapping (make-corner-mapping-simple top left)]
          (when (and (= corner :top-left) (= (+ 1 i) 3))
            (pp [corner
               (+ i 1)
               corner-mapping
               (+ 1 (power-two (. corner-neighbor-order corner) truth-row))
               (. corner-neighbor-order corner)
               truth-row]))
          (. corner-mapping (+ 1 (power-two (. corner-neighbor-order corner) truth-row)))
          )))))

  (local (unique-quad-layouts unique-quad-layouts-map) (dedupe-array quad-maps))

   {: image : quads : unique-quad-layouts : unique-quad-layouts-map : neighbor-order}
  )

(fn draw-unique-tileset [image quads unique-quad-layouts unique-quad-layouts-map]
  (local lg love.graphics)
  (lg.push)
  (lg.scale 4)
  (each [i quad-layout (ipairs unique-quad-layouts)]
    (local [top-right top-left bottom-left bottom-right] quad-layout)
    (lg.draw image (. quads top-right 1) 0 0)
    (lg.draw image (. quads top-left 1) 8 0)
    (lg.draw image (. quads bottom-left 1) 8 8)
    (lg.draw image (. quads bottom-right 1) 0 8)
    (lg.translate 16 0)
    (when (= (% i 16) 0)
      (lg.translate -256 16))    
    )
  (lg.pop)
  (lg.reset)
  (each [i _ (ipairs unique-quad-layouts-map)]
    (lg.print i (* (% (- i 1) 16) 16 4) (* (math.floor (/ (- i 1) 16)) 16 4))))

(fn get-unique-quad-layouts-map-index [subtile i j grid]
  ;; (local neighbor-order [:top-right :top :top-left :left :bottom-left :bottom :bottom-right :right :center])
  ;; avoid hash lookup in inner loop
  (local indexes [-1 -1 0 -1 1 -1 1 0 1 1 0 1 -1 1 -1 0 0 0])
  (var total 0)
  (for [k 1 (# indexes) 2]
    (let [x (+ i (. indexes k))
          y (+ j (. indexes (+ k 1)))
          v (. grid x y)]
      (set total (+ total (* v  (^ 2 (/ (- i 1) 2)))))))
  total)

(fn draw-grid [imaage quads])

{: truth-table-row : make-simple-square-tileset : draw-unique-tileset}
