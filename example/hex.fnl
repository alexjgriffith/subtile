(fn hex [hex]
  "Takes either a hex as a string with 6 (:rrggbb) chars or 8 chars (:rrggbbaa).
In the case where there are 6 chars the final two alpha chars are set to ff.

(import-macros {: hex} :hex)
(hex :0d2b45)
(hex :203c5633)
"
  (local hex-map {:0 0 :1 1 :2 2 :3 3 :4 4 :5 5 :6 6 :7 7 :8 8 :9 9
                  :a 10 :b 11 :c 12 :d 13 :e 14 :f 15
                  :A 10 :B 11 :C 12 :D 13 :E 14 :F 15})
  (let [hex8 (if (= (# hex) 8) hex (.. hex "ff"))]
    (fcollect [i 1 8 2]
      (/ (+ (* 16 (. hex-map (string.sub hex8 i i)))
            (. hex-map (string.sub hex8 (+ i 1) (+ i 1)))) 256))))

{: hex}
