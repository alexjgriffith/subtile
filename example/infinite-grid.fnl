

;;(fcollect [i 0 3] [(-> i (% 2)) (-> i (% 4) (/ 2) (math.floor)) (-> (+ i 1) (% 2) (* 2) (- 1))])

(fn index-to-ra [index]
  (let [r (-> index (/ 4) (math.sqrt) (math.floor) (+ 1))
        end (* 4 (^ r 2))
        circ (* 4 (1 + (* 2 (- r 1))))
        a (- index (- end circ))]
    (values r a)))

(fn ra-to-index [r a]
  (let [end (* 4 (^ r 2))
        circ (* 4 (1 + (* 2 (- r 1))))
        index (+ a (- end circ))]
    index))

;; (fcollect [i 0 3] [(-> i (% 2)) (-> i (% 4) (/ 2) (math.floor)) (-> (+ i 1) (% 2) (* 2) (- 1))])

(fn ra->ij [r a]
  (let [circ (* 4 (1 + (* 2 (- r 1))))
        ;; start (* 4 (^ r 2))
        side-length (-> circ (/ 4) (math.floor))
        side (-> a (/ side-length) (math.floor)) ;; {0,3}
        index (- a (* side-length side)) ;; remainder
        ip (-> side (% 2))
        jp (-> side (% 4) (/ 2) (math.floor))
        t (-> ip (* 2) (- 1) (* r))
        r (-> jp (* 2) (- 1) (* r))
        sign (-> (+ i 1) (% 2) (* 2) (- 1))
        di (* sign (* index ip))
        dj (* sign (* index jp))
        i (+ di t)
        j (+ dj r)
        ]
    (values i j)
    ))

(fn ij->ra [i j]
  (let [r (math.max (math.abs i) (math.abs j))
        end (* 4 (^ r 2))
        circ (* 4 (1 + (* 2 (- r 1))))
        
        ]))

(fn make-grid []
  (let [grid [[] [] [] []]]
    (setmetatable grid infinite-grid)))

make-grid
